from django import forms
from django.forms import ModelForm
from recursos.models import Recurso


class RecursoForm(ModelForm):
    class Meta:
        model = Recurso
        fields =[
        'nombre',
        'categoria',
        'codigo',
        'marca',
        'serie',
        ]
