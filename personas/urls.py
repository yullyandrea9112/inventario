from django.conf.urls import url, include
from personas.views import PersonCreateView, PersonListView, PersonUpdateView, PersonDetailView

urlpatterns = [
    url(r'^registro$', PersonCreateView.as_view(), name='create-person'),
    url(r'^lista$', PersonListView.as_view(), name='list-person'),
    url(r'^(?P<id>[-\w]+)/update/$', PersonUpdateView.as_view(), name='update-person'),
    url(r'^(?P<id>[-\w]+)/$', PersonDetailView.as_view(), name='detail-person'),

]
