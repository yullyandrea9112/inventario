Aplicación web para la gestión de inventarios
La aplicación permite registrar personas y recursos y asignar los recursos a una personas

**python3
Django11
Bootstrap**

### Installing
* Crear ambiente virtual (virtualen o virtualwrapper)
* Ingresar al ambiente virtual: source ambien/bin/activate
* clonar proyecto: git clone https://gitlab.com/yullyandrea9112/inventario.git
* cd inventario/
* Instalar requerimientos:  **pip install -r requirements.txt**
* python manage.py runserver
