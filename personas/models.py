# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.db import models
from recursos.models import Recurso


class Persona(User):
    identificacion = models.CharField(blank=True, max_length=100, verbose_name='Identificación')
    recursos = models.ManyToManyField(Recurso, blank=True)
