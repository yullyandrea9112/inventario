from django import forms
from django.forms import ModelForm
from personas.models import Persona
from recursos.models import Recurso

class PersonaForm(ModelForm):
    class Meta:
        model = Persona
        exclude=['is_active', 'last_login', 'groups', 'user_permissions', 'is_staff', 'date_joined', 'is_superuser','password1', 'password2', 'password', 'address', 'address_city',
             'phone', 'other_phone', 'profession', 'username']

        fields = ['first_name', 'last_name', 'identificacion']

        labels = {
            'first_name': 'Nombres',
            'last_name': 'Apellidos'
        }

class PersonaUpdateForm(ModelForm):
    recursos =forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple, queryset=Recurso.objects.all(), required=True)
    class Meta:
        model = Persona
        fields = ['recursos']
