from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from personas.models import Persona
from personas.forms import PersonaForm, PersonaUpdateForm
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView


class PersonCreateView(CreateView):
    model = Persona
    form_class = PersonaForm
    template_name ='personas/registro_persona.html'
    success_url = reverse_lazy('personas:list-person')

    def form_valid(self, form):
        user = form.save(commit=False)
        user.username = user.first_name
        form.save()
        return super(PersonCreateView, self).form_valid(form)

class PersonListView(ListView):
    model = Persona
    template_name = 'personas/lista_personas.html'
    paginate_by = 10
    context_object_name = 'personas'

    def get_queryset(self):
        qs = Persona.objects.all()
        return qs

class PersonUpdateView(UpdateView):
    model = Persona
    form_class= PersonaUpdateForm
    template_name = 'personas/asignacion.html'
    success_url = reverse_lazy('personas:list-person')

    def get_object(self, queryset=None):
        obj = Persona.objects.get(pk=self.kwargs['id'])
        return obj

class PersonDetailView(DetailView):
    model = Persona
    template_name = 'recursos/lista_recursos.html'

    def get_object(self, queryset=None):
        obj = Persona.objects.get(pk=self.kwargs['id'])
        return obj

    def get_context_data(self, **kwargs):
        context = super(PersonDetailView, self).get_context_data(**kwargs)
        person = Persona.objects.get(id=self.kwargs['id'])
        recursos = person.recursos.all() if person.recursos else None
        is_detail = True
        context['recursos'] = recursos
        context['person'] = person
        context['is_detail'] = is_detail
        return context
