from django.db import models

# Create your models here.
class Recurso(models.Model):
    nombre = models.CharField(max_length=45)
    categoria = models.CharField(max_length=45, blank=True, null=True)
    codigo = models.CharField(blank=True, max_length=45)
    marca = models.CharField(blank=True, max_length=45)
    serie = models.CharField(blank=True, max_length=45)
    def __str__(self):
        return str(self.nombre)
