from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from recursos.models import Recurso
from recursos.forms import RecursoForm
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView

# Create your views here.
class RecursoCreateView(CreateView):
    model=Recurso
    form_class = RecursoForm
    template_name = 'recursos/registro_recurso.html'
    success_url = reverse_lazy('recursos:list-recurso')


class RecursoListView(ListView):
    model = Recurso
    template_name = 'recursos/lista_recursos.html'
    paginate_by = 10
    context_object_name = 'recursos'

    def get_queryset(self):
        qs = Recurso.objects.all()
        return qs


class RecursoUpdateView(UpdateView):
    model = Recurso
    form_class= RecursoForm
    template_name = 'recursos/registro_recurso.html'
    success_url = reverse_lazy('recursos:list-recurso')

    def get_object(self, queryset=None):
        obj = Recurso.objects.get(pk=self.kwargs['id'])
        return obj
