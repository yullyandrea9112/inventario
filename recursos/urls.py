from django.conf.urls import url, include
from recursos.views import *

urlpatterns = [
    url(r'^registro/inventario$', RecursoCreateView.as_view(), name='create-recurso'),
    url(r'^lista/inventario$', RecursoListView.as_view(), name='list-recurso'),
    url(r'^(?P<id>[-\w]+)/update/$', RecursoUpdateView.as_view(), name='update-recurso'),
]
